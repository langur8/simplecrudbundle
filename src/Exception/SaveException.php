<?php

namespace dlouhy\SimpleCRUDBundle\Exception;

/**
 * Description of WTPServiceErrorException
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */
class SaveException extends \Exception
{
	
	/**
	 * @var array
	 */
	private $formErrors;
	
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
	 * @param int        $code     The internal exception code
     * @param \Exception $previous The previous exception
     */
    public function __construct($message, $code, $previous, $errors)
    {
		parent::__construct($message, $code, $previous);
		$this->formErrors = $errors;
    }	
	
	function getFormErrors()
	{
		return $this->formErrors;
	}


	function setFormErrors($formErrors)
	{
		$this->formErrors = $formErrors;
	}

	
}
