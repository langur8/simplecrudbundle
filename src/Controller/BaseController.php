<?php

namespace dlouhy\SimpleCRUDBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Form;
use dlouhy\SimpleCRUDBundle\Exception\SaveException;
use Symfony\Component\Form\FormError;

abstract class BaseController extends Controller
{

	/**
	 * Nazev entity vc. namespace
	 *
	 * @var string
	 */
	protected $sEntity;

	/**
	 * Nazev editacniho formulare
	 *
	 * @var string
	 */
	protected $sForm;

	/**
	 * Nazev entity balicku kratky
	 *
	 * @var string
	 */
	protected $sBundle;

	/**
	 * Nazev controlleru
	 *
	 * @var string
	 */
	protected $sController;

	/**
	 * Nazev akce
	 *
	 * @var string
	 */
	protected $sAction;

	/**
	 * Umisteni sablony s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sTemplate;

	/**
	 * Umisteni sablony formulare s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sFormTemplate;

	/**
	 * Umisteni sablony formulare s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sTableTemplate;

	/**
	 * @var boolean
	 */
	protected $checkAccess = false;
	
	
	protected $redirect;


	protected function listAction(Request $request)
	{
		$this->init($request);
		$records = $this->findAll();
		if ($this->checkAccess === true) {
			foreach ($records as $key => $record) {
				if (!$this->isGranted('view', $record)) {
					unset($records[$key]);
				}
			}
		}

		return $this->render($this->sTemplate, array('records' => $records, 'tree' => $this instanceof BaseCRUDTreeController));
	}


	public function addAction(Request $request)
	{
		$this->init($request);
		$entity = $this->createEntity();

		if ($this->checkAccess === true) {
			$this->checkAccess('add', $entity);
		}

		if($this->redirect === null) {
			$this->redirect = $this->generateUrl(str_replace('_add', '_save', $request->get('_route')));
		}
		
		$form = $this->getForm($entity, $this->redirect);

		return $this->render($this->sTemplate, array('form' => $form->createView()));
	}


	public function viewAction(Request $request, $id)
	{
		$this->init($request);

		$entity = $this->find($id);

		if (!$entity instanceof $this->sEntity) {
			throw $this->createNotFoundException('Not found');
		}

		if ($this->checkAccess === true) {
			$this->checkAccess('view', $entity);
		}

		return $this->render($this->sTemplate, array('record' => $entity));
	}


	public function editAction(Request $request, $id)
	{
		$this->init($request);

		$entity = $this->find($id);
		if (!$entity instanceof $this->sEntity) {
			throw $this->createNotFoundException('Not found');
		}	

		if ($this->checkAccess === true) {
			$this->checkAccess('edit', $entity);
		}

		if($this->redirect === null) {
			$this->redirect = $this->generateUrl(str_replace('_edit', '_save', $request->get('_route')), array('id' => $id));
		}
		
		$form = $this->getForm($entity, $this->redirect);

		return $this->render($this->sTemplate, array('form' => $form->createView()));
	}


	public function saveAction(Request $request, $id)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
		}

		$this->init($request);

		if ($id > 0) {
			$entity = $this->find($id);
			if (!$entity instanceof $this->sEntity) {
				throw $this->createNotFoundException('Not found');
			}
			if ($this->checkAccess === true) {
				$this->checkAccess('edit', $entity);
			}
		} else {
			$entity = $this->createEntity();
		}
		$form = $this->getForm($entity, $this->generateUrl($request->get('_route')));
		$form->handleRequest($request);		

		$message = '';
		$returnCode = 400;				
		
		if ($form->isValid()) {
			try {
				$this->save($entity);
				if($this->redirect === null) {
					$listRoute = str_replace('_save', '_list', $request->get('_route'));
					$this->redirect = $this->generateUrl($listRoute);
				} else {
					$this->redirect = str_replace('--id--', $entity->getId(), $this->redirect);
				}
				$returnCode = 200;
			} catch(SaveException $e) {
				$this->processSaveException($form, $e);
			} 
		}

		return new JsonResponse(array(
			'message' => $message,
			'replace' => true,
			'redirect' => $this->redirect,
			'html' => $this->renderView($this->sFormTemplate, array(
				'form' => $form->createView(),
				'includeJs' => true,
				'errors' => $this->get('form.form_errors')->getArray($form)
			))), $returnCode);
	}


	public function deleteAction(Request $request, $id)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
		}

		$this->init($request);

		$entity = $this->find($id);
		if (!$entity instanceof $this->sEntity) {
			throw $this->createNotFoundException('Not found');
		}

		if ($this->checkAccess === true) {
			$this->checkAccess('delete', $entity);
		}

		$this->delete($entity);
		
		return new JsonResponse(array(
			'elementId' => 'data-table-content',
			'html' => $this->renderView($this->sTableTemplate, array(
				'records' => $this->findAll(),
				'includeJs' => true
			))), 200);		
		
	}


	protected function switchBoolAction(Request $request, $id, $property)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
		}

		if (!$id) {
			throw $this->createNotFoundException('Nepredano ID');
		}

		$this->init($request);
		
		$record = $this->find($id);

		if (!$record) {
			return new JsonResponse(array('id' => $id), 400);
		}

		if ($this->checkAccess === true) {
			$this->checkAccess('edit', $record);
		}

		$setter = 'set' . ucfirst($property);
		$getter = 'get' . ucfirst($property);
		if (!method_exists($record, $getter)) {
			$getter = 'is' . ucfirst($property);
		}

		$record->$setter(!$record->$getter());

		$this->save($record);

		return new JsonResponse(array(
			'elementId' => 'data-table-content',
			'html' => $this->renderView($this->sTableTemplate, array(
				'records' => $this->findAll(),
				'includeJs' => true
			))), 200);
	}
	
	
	protected function moveAction(Request $request, $id, $property, $direction)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
		}

		if (!$id) {
			throw $this->createNotFoundException('Nepredano ID');
		}

		$this->init($request);
		
		$record = $this->find($id);

		if (!$record) {
			return new JsonResponse(array('id' => $id), 400);
		}

		if ($this->checkAccess === true) {
			$this->checkAccess('edit', $record);
		}

		$setter = 'set' . ucfirst($property);
		$getter = 'get' . ucfirst($property);
		if (!method_exists($record, $getter)) {
			$getter = 'is' . ucfirst($property);
		}		
		
		$record->$setter($direction ? $record->$getter()+1 : $record->$getter()-1);

		$this->save($record);

		return new JsonResponse(array(
			'elementId' => 'data-table-content',
			'html' => $this->renderView($this->sTableTemplate, array(
				'records' => $this->findAll(),
				'includeJs' => true
			))), 200);
	}			


	protected function init(Request $request)
	{
		$matches = array();
		$controller = $request->attributes->get('_controller');
		preg_match('/(.*)\\\Controller\\\(.*)Controller::(.*)Action/', $controller, $matches);

		if ($this->sBundle === null) {
			$this->sBundle = str_replace("\\", '', $matches[1]);
		}
		if ($this->sEntity === null) {
			$this->sEntity = $matches[1] . '\Entity\\' . $matches[2];
		}
		if ($this->sForm === null) {
			$this->sForm = $matches[1] . '\Form\Type\\' . $matches[2] . 'Type';
		}
		if ($this->sController === null) {
			$this->sController = $matches[2];
		}
		if ($this->sAction === null) {
			$this->sAction = $matches[3];
		}
		if ($this->sTemplate === null) {
			$this->sTemplate = $this->sBundle . ':' . $this->sController . ':' . $this->sAction . '.html.twig';
		}
		if ($this->sFormTemplate === null) {
			$this->sFormTemplate = $this->sBundle . ':' . $this->sController . ':form.html.twig';
		}
		if ($this->sTableTemplate === null) {
			$this->sTableTemplate = $this->sBundle . ':' . $this->sController . ':table.html.twig';
		}
	}


	protected function findAll()
	{
		$repo = $this->getDoctrine()->getRepository($this->sEntity);
		return $repo->findAll();
	}
	
	
	protected function find($id)
	{
		$repo = $this->getDoctrine()->getRepository($this->sEntity);
		return $repo->find($id);
	}
	

	protected function save($entity)
	{
		$this->checkValidity($entity);
		$em = $this->getDoctrine()->getManager();
		$em->persist($entity);
		$em->flush();
	}
	
	
	protected function delete($entity) {
		$em = $this->getDoctrine()->getManager();
		$em->remove($entity);
		$em->flush();		
	}


	protected function checkAccess($action, $record)
	{
		$this->denyAccessUnlessGranted($action, $record, 'Unauthorized access!');
	}
	
	
	protected function removeEmptyTranslations($entity)
	{
		$em = $this->getDoctrine()->getManager();
		foreach($entity->getTranslations()->getIterator() as $translation) {
			if($entity->getDefaultLocale() !== $translation->getLocale() && method_exists($translation, 'isEmpty') && $translation->isEmpty()) {
				$em->remove($translation);
			}
		}		
	}
	
	
	protected function getForm($entity, $action, $options = array())
	{
		return $this->createForm($this->sForm, $entity, array('action' => $action) + $options);
	}			
	
	
	protected function processSaveException($form, $e)
	{
		foreach($e->getFormErrors() as $list) {
			foreach($list->getIterator() as $constraintViolation) {		
				if($constraintViolation->getPropertyPath()) {										
					$path = explode('.', $constraintViolation->getPropertyPath());
					foreach($path as $p) {
						$form = $form->get($p);
					}				
					$form->addError(new FormError($constraintViolation->getMessage()));
				}
			}
		}				
	}
	
	
	protected function createEntity()
	{
		return new $this->sEntity;
	}
	

	protected function checkValidity($entity)
	{	
	}
	
}
