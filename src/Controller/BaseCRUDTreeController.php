<?php

namespace dlouhy\SimpleCRUDBundle\Controller;
use Symfony\Component\Form\FormError;
use dlouhy\SimpleCRUDBundle\Exception\SaveException;

abstract class BaseCRUDTreeController extends BaseController
{

	protected function setParent($entity)
	{
		if ($entity->getParent() !== null && $entity->getParent() instanceof $this->sEntity) {
			if($entity->getId() === $entity->getParent()->getId()) {
				throw new \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException('Reference error.');
			}			
			$entity->setChildNodeOf($entity->getParent());			
		} else {
			//nastavime roota
			$entity->setMaterializedPath('');		
		}
			
	}
	
	
	protected function save($entity)
	{
		$this->checkValidity($entity);
		
		$em = $this->getDoctrine()->getManager();				
		$em->persist($entity);		
		$em->flush();		
		
		$repo = $this->getDoctrine()->getRepository($this->sEntity);		
		$em = $this->getDoctrine()->getManager();
		foreach($repo->getSubTree($entity)->getIterator() as $save) {
			$save->setPath(null);
			$em->persist($save);
		}								
		$em->flush();						
		
		$this->setParent($entity);		
		$em->persist($entity);
		$em->flush();
	}
	
	
	protected function delete($entity)
	{
		$repo = $this->getDoctrine()->getRepository($this->sEntity);	
		$em = $this->getDoctrine()->getManager();
		$a = $repo->getSubTree($entity);
		foreach($repo->getSubTree($entity)->getIterator() as $dEntity) {
			$em->remove($dEntity);
		}
		$em->flush();
	}	


	protected function findAll()
	{
		$repo = $this->getDoctrine()->getRepository($this->sEntity);
		return $repo->getRootNodesInitialized();
	}
	
	
	protected function find($id)
	{
		$entity = parent::find($id);
		
		if($entity instanceof $this->sEntity && $entity->isRootNode() === false) {
			$repo = $this->getDoctrine()->getRepository($this->sEntity);
			$parent = $repo->getTree($entity->getMaterializedPath());		
			$entity->setChildNodeOf($parent);
			$entity->setParent($parent);
		}
		return $entity;
	}	
	
	
	protected function getForm($entity, $action, $options = array())
	{
		$repo = $this->getDoctrine()->getRepository($this->sEntity);
		return $this->createForm(new $this->sForm($repo->getTreeSorted()), $entity, array('action' => $action) + $options);
	}
		
	
	protected function checkValidity($entity)
	{
		$errors = $this->get('validator')->validate($entity);

		if(!empty($errors)) {
			throw new SaveException('Invalid entity', 0, null, $errors);
		}			
	}	
	
	
	/**
	 * propise chybu do formulare, neni kompleptne poreseno co je v prekladu co ne apod.
	 * 
	 * @param \Symfony\Component\Form\AbstractType $form
	 * @param \dlouhy\SimpleCRUDBundle\Exception\SaveException $e
	 */
	protected function processSaveException($form, $e)
	{		
		foreach($e->getFormErrors() as $list) {
			foreach($list->getIterator() as $constraintViolation) {
				$path = $constraintViolation->getPropertyPath();
				if(in_array($path, array('locale', 'path'))) {
					$path = 'slug';
				} 
				$form->get('translations')->get('cs')->get($path)->addError(new FormError($constraintViolation->getMessage()));
			}
		}		
		
	}	

}
