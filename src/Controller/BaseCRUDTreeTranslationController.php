<?php

namespace dlouhy\SimpleCRUDBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use dlouhy\SimpleCRUDBundle\Exception\SaveException;


abstract class BaseCRUDTreeTranslationController extends BaseCRUDTreeController
{

	/**
	 * Nazev entity vc. namespace
	 *
	 * @var string
	 */
	protected $sProxyEntity;
	
	/**
	 * @var array locales
	 */
	protected $locales = false;		


	protected function init(Request $request)
	{
		$this->locales = $this->getParameter('locales_arr');		
		return parent::init($request);				
	}
	
	protected function save($entity)
	{	
		$em = $this->getDoctrine()->getManager();				
		$em->persist($entity);
		$this->checkValidity($entity);
		$em->flush();
		
		$this->setParent($entity);		
		
		$repo = $this->getDoctrine()->getRepository($this->sEntity);		
		
		foreach($repo->getSubTree($entity)->getIterator() as $save) {
			foreach($save->getTranslations() as $trans) {
				$trans->setPath(null);				
			}								
			
			$em->persist($save);
			$this->checkValidity($entity);			
		}
		
		$em->flush();
	
		$this->removeEmptyTranslations($entity);
		$em->flush();
	}
	
	
	protected function checkValidity($entity)
	{
		$entity->preSave();
		
		$errors = array();		
		$errors[] = $this->get('validator')->validate($entity);
		foreach($entity->getTranslations() as $trans) {			
			$errors[] = $this->get('validator')->validate($trans);
		}
		
		$exErrors = array();
		foreach($errors as $errorList) {
			if($errorList->count() > 0) {
				$exErrors[] = $errorList;
			}			
		}
		
		if(!empty($exErrors)) {
			throw new SaveException('Invalid entity', 0, null, $exErrors);
		}		
	}


}
