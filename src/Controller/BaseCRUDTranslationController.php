<?php

namespace dlouhy\SimpleCRUDBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use dlouhy\SimpleCRUDBundle\Exception\SaveException;


abstract class BaseCRUDTranslationController extends BaseController
{

	/**
	 * Nazev entity vc. namespace
	 *
	 * @var string
	 */
	protected $sProxyEntity;
	
	/**
	 * @var array locales
	 */
	protected $locales = false;		


	protected function init(Request $request)
	{
		$this->locales = $this->getParameter('locales_arr');		
		return parent::init($request);				
	}
	
	protected function save($entity)
	{
		$this->checkValidity($entity);
		$em = $this->getDoctrine()->getManager();
		$em->persist($entity);		
		$this->removeEmptyTranslations($entity);		
		$em->flush();
	}	
	
	protected function checkValidity($entity)
	{	
		
		$errors = array();		
		$errors[] = $this->get('validator')->validate($entity);
		foreach($entity->getTranslations() as $trans) {			
			$errors[] = $this->get('validator')->validate($trans);
		}
		
		$exErrors = array();
		foreach($errors as $errorList) {
			if($errorList->count() > 0) {
				$exErrors[] = $errorList;
			}			
		}
		
		if(!empty($exErrors)) {
			throw new SaveException('Invalid entity', 0, null, $exErrors);
		}		
	}	


}
